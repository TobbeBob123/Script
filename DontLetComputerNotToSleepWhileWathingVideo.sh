#!/usr/bin/env sh

xset -dpms
xset s off
echo "Screen blanking disabled for $1m"
sleep $1m
echo "Screen blanking re-enabled"
xset dpms
xset s on
